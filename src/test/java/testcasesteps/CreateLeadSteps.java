package testcasesteps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	public static ChromeDriver driver;
	@Given("Open the chrome browser")
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver=new ChromeDriver();
	}
	@And("Maximize the browser")
public void maxBrowser()
{
	driver.manage().window().maximize();
}
@And("Set timeout")
public void setTimeout()
{
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
@And("Hit the url as (.*)")
public void Hitur(String url)
{
driver.get(url);
}
@And("Enter the user name as (.*)")
public void Enteruname(String uname)
{
	driver.findElementById("username").sendKeys(uname);
}
@And("enter the password as (.*)")
public void enterpwd(String pwd)
{
	driver.findElementById("password").sendKeys(pwd);

}
@And("click login")
public void clickLogin()
{
	driver.findElementByClassName("decorativeSubmit").click();
}
@And("click crmsfa")
public void clickcrmsfs()
{
	driver.findElementByLinkText("CRM/SFA").click();
}
@And("click create Lead")		
public void clickCreateLead()
{
	driver.findElementByLinkText("Create Lead").click();

}
@And("Enter the company name as (.*)")
public void Companyname(String cname)
{
	driver.findElementById("createLeadForm_companyName").sendKeys(cname);
}
@And("Enter the First name as (.*)")
public void Firstname(String fname)
{

driver.findElementById("createLeadForm_firstName").sendKeys(fname);
}
@And("Enter the last name as (.*)")
public void Enterlastname(String lname)
{
	driver.findElementById("createLeadForm_lastName").sendKeys(lname);
}

@When("click on the create lead button")
public void CreateLead()
{
	driver.findElementByClassName("smallSubmit").click();
}

@Then("verify lead is created successfully")
public void message()
{
	System.out.println("Create lead successfull");
}

/*@Then("verify lead is created will fail")
public void failed()
{
	System.out.println("Create lead failed");
}*/
		

}
