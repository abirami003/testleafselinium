package utils;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporter {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName,testCaseDesc,category,author,ExcelFileName;
	public void beginResult() {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/result2.html");
	    html.setAppendExisting(false);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}	
	
	public void startTestCase() {
		test = extent.createTest(testCaseName, testCaseDesc);
		test.assignCategory(category);
		test.assignAuthor(author);
	}
	
	public void reportStep(String desc,String status) {
		if(status.equalsIgnoreCase("Pass")) {
			test.pass(desc);			
		} else if(status.equalsIgnoreCase("Fail")) {
			test.fail(desc);			
		} else if(status.equalsIgnoreCase("Warning")) {
			test.warning(desc);			
		}		
	}	
	public void reportStep(String desc,String status,String snapPath) throws IOException {
		if(status.equalsIgnoreCase("Pass")) {
			test.pass(desc,MediaEntityBuilder.createScreenCaptureFromPath(snapPath).build());			
		} else if(status.equalsIgnoreCase("Fail")) {
			test.fail(desc,MediaEntityBuilder.createScreenCaptureFromPath(snapPath).build());			
		} else if(status.equalsIgnoreCase("Warning")) {
			test.warning(desc,MediaEntityBuilder.createScreenCaptureFromPath(snapPath).build());			
		}		
	}
	public void endResult() {
		extent.flush();
	}
	
	
	
	
	
	
	
	
	
	
	
}















