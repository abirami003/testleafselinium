package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
//public static String inputFileName;
	public static Object[][]  getExcelData(String inputFileName) throws IOException {
		
		XSSFWorkbook book=new XSSFWorkbook("./Data/"+inputFileName+".xlsx");
		XSSFSheet sheet1 = book.getSheetAt(0);
		int lastRowNum = sheet1.getLastRowNum();
		XSSFRow row2 = sheet1.getRow(1);
		short lastCellNum2 = row2.getLastCellNum();
		System.out.println(lastRowNum);
		Object[][] data = new Object[lastRowNum][lastCellNum2];
		for (int i = 1; i <=lastRowNum; i++) {
			System.out.println();
			XSSFRow row = sheet1.getRow(i);
			int lastCellNum = row.getLastCellNum();
			for (int j = 0; j <lastCellNum; j++) {
			
				XSSFCell cell = row.getCell(j);
			
				data [i-1][j] =cell.getStringCellValue();
				
			}
		}
		
		return data;

	}

}
