package wwek1.day1.homeWork;

import java.util.Scanner;

public class PrimeNumbers {

	public static void main(String[] args) {
		
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter the range");
		int range=scr.nextInt();
		int count;
		System.out.println("Prime numbers between 1 to "+range+" is");
		for(int i=2;i<=range;i++)
		{
			count=0;
			for(int j=2;j<=i/2;j++)
			{
				
				if(i%j==0)
				{
					count++;
					break;
				}
				
			}
			if(count == 0)
			{
				System.out.println(i);
			}
	}
}
}