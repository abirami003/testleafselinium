package wwek1.day2;

import java.util.Scanner;

public class StringFun {
	//Program To find the no of occurrence of char 'a' in a given String

	public static void main(String[] args) {
		int count=0;
		System.out.println("Enter the string");
		Scanner scr=new Scanner(System.in);
		String text=scr.nextLine();
		char ch[]=text.toCharArray();
		for(char eachChar:ch)	
		{
		if(eachChar =='a')
		{
			count++;
		}

	}
		System.out.println("No of occurence of 'a' in a String "+text+" is "+count);
	}
}
