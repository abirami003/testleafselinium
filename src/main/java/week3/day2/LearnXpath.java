package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnXpath {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/");
		driver.findElementByXPath("//img[@alt='Table']").click();
		List<WebElement> checkBox = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println("No of check boxes "+checkBox.size());
		
		WebElement second = checkBox.get(checkBox.size()-1);
		second.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		second.click();
		
		driver.findElementByXPath("(//font[contains(text(),'80')]/following::input)[1]").click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		driver.findElementByXPath("(//font[contains(text(),'80')]/following::input)[1]").click();
		
		
			driver.close();
		}
		
	}


