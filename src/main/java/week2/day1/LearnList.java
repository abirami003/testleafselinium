package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		
		List<String> Myphones=new ArrayList<String>();
		Myphones.add("Redmi Note 3");
		Myphones.add("Redmi Note 5A");
		Myphones.add("Redmi Note 4");
		Myphones.add("Redmi");
		
		System.out.println("Count of mobiles is "+Myphones.size());
		
		System.out.println("Latest mobile is "+Myphones.get(Myphones.size()-1));
		Collections.sort(Myphones);
		for (String eachphones : Myphones) {
			System.out.println(eachphones);
		}
		
	}

}
