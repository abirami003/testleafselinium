package week2.day1;
import java.util.*;

public class ListAndStringExercise {

	public static void main(String[] args) {

		System.out.println("Enter the string");
		Scanner scr=new Scanner(System.in);
		String str=scr.nextLine();
		//converting the string to char array
		char ch[]=str.toCharArray();
		//creating a new list
		List <Character> li=new ArrayList <Character> ();
		// for loop to add the char array ch to the list
		for (char eachch : ch) {
			li.add(eachch);
			}
		// To print the list in sorted order
		Collections.sort(li);
		System.out.println("Sorted order string");
		for (Character eachli : li) {
			System.out.println(eachli);
			
		}
		// To print the list in Reverse sorted order
		Collections.reverse(li);
		System.out.println("Reverse Sorted order string");
		for (Character eachli : li) {
			System.out.println(eachli);
		
		}
		// To remove duplicates in the list and then print
		for(int i=0;i<li.size();i++){
			 
			 for(int j=i+1;j<li.size();j++){
			            if(li.get(i)==li.get(j)){
			                li.remove(j);
			                j--;
			            }
			    }
		System.out.println("duplicates removed");
			 for (Character eachli : li) {
					System.out.println(eachli);
				
				}
		
		}
		}
}
