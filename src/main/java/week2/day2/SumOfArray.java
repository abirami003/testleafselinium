package week2.day2;

import java.util.Scanner;

public class SumOfArray {

	public static void main(String[] args) {
		int sum=0;
		System.out.println("Enter the Size");
		Scanner scr=new Scanner(System.in);
		int size = scr.nextInt();
		int a[]=new int[size];
		System.out.println("Enter "+size+" values");
		for(int i=0;i<size;i++)
		{
			a[i]=scr.nextInt();
			sum=sum+a[i];
		}

		System.out.println("Sum of elements in array is "+sum);
	}

}
