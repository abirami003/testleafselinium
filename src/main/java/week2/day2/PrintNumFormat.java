package week2.day2;

import java.util.Scanner;

public class PrintNumFormat {

	public static void main(String[] args) {
		System.out.println("Enter two values");
		Scanner scr=new Scanner(System.in);
		int n1=scr.nextInt();
		int n2=scr.nextInt();
		for(int i=n1;i<=n2;i++) {
			if((i%3==0)&&(i%5==0)&&(i!=0))
			{
				System.out.print(" FIZZBUZZ ");
			}
			else if(i%3==0&&(i!=0))
			{
				System.out.print(" FIZZ ");
			}
			else if(i%5==0&&(i!=0))
			{
				System.out.print(" BUZZ ");
			}
			else
			{
				System.out.print(" "+i+" ");
			}
		}

	}

}
