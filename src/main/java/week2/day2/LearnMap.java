package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LearnMap {

	public static void main(String[] args) {
		System.out.println("Enter your name here");
		Scanner scr=new Scanner(System.in);
		String str=scr.nextLine();
		char ch[]=str.toCharArray();
		Map<Character,Integer> mp=new HashMap<Character,Integer> ();
		for (char eachchar : ch) {
		if(mp.containsKey(eachchar))
		{	Integer count=mp.get(eachchar)+1;
			mp.put(eachchar,count );
			System.out.println("Duplicate char in the name '"+eachchar+"'");
		}
		else
		{
			mp.put(eachchar,1 );
		}
	}
	}
}