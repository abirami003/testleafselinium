package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
/*Public void learnset()
{
	
}*/
public class LearnSets {

	public static void main(String[] args) {
		// create a set follows Hash algorithm
		Set <String> mobiles=new HashSet<String> ();
		mobiles.add("Redmi");
		mobiles.add("Lenova");
		mobiles.add("Redmi");
		mobiles.add("Samsung");
		for (String eachMobile : mobiles) {
			System.out.println(eachMobile);
		}
		//Using linked Hash set to retain the order
		Set <String> phones=new LinkedHashSet<String> ();
		phones.add("Redmi");
		phones.add("Lenova");
		phones.add("Redmi");
		phones.add("Samsung");
		
		int size = phones.size();
		System.out.println(size);
		//Adding all element in linked list to Array list
		List <String> ph=new ArrayList<String> ();
		ph.addAll(phones);
		
		for (String eachphone : ph) {
			System.out.println(eachphone);
		}
		int size2 = ph.size();
		System.out.println("Last mobile in the list is"+ph.get(size2-1));
		
	}

}
