package POM_pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;



public class LoginPage extends ProjectMethods
{
public LoginPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(id="username")
WebElement elementUserName;
public LoginPage enterUserName(String data) throws IOException
{
	type(elementUserName, data);
	return this;
}

@FindBy(how=How.ID,using ="password")
WebElement elementPassword;
public LoginPage enterPassword(String data) throws IOException
{
	type(elementPassword, data);
	return this;
}

@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
WebElement elementSubmit;
public HomePage clickLogin() {
click(elementSubmit);
return new HomePage();
}
}