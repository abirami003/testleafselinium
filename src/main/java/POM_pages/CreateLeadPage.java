package POM_pages;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;



import wdMethods.ProjectMethods;


public class CreateLeadPage extends ProjectMethods
{
public CreateLeadPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(id="createLeadForm_companyName")
WebElement clCompanyName;
public CreateLeadPage enterCompanyName(String data)  throws IOException
{
	type(clCompanyName, data);
	return this;
}

@FindBy(how=How.ID,using ="createLeadForm_firstName")
WebElement clFirstName;
public CreateLeadPage enterFName(String data) throws IOException
{
	type(clFirstName, data);
	return this;
}

@FindBy(id="createLeadForm_lastName")
WebElement clLastName;
public CreateLeadPage enterLName(String data) throws IOException
{
	type(clLastName, data);
	return this;
}

@FindBy(how=How.ID,using ="createLeadForm_primaryEmail")
WebElement clEmail;
public CreateLeadPage enterEmail(String data) throws IOException
{
	type(clEmail, data);
	return this;
}

@FindBy(how=How.ID,using ="createLeadForm_primaryPhoneNumber")
WebElement clPhoneNumber;
public CreateLeadPage enterPhoneNumber(String data) throws IOException
{
	type(clPhoneNumber, data);
	return this;
}

@FindBy(how=How.NAME,using="submitButton")
WebElement elementCreateLead;
public ViewLeadPage clickCreateLead() {
click(elementCreateLead);
return new ViewLeadPage();
}
}