package POM_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods
{
public MyLeadsPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(how= How.LINK_TEXT,using="Create Lead")
WebElement linkCreateLead;
public CreateLeadPage clickCreateLeadLink()
{
	click(linkCreateLead);
	return new CreateLeadPage();
	
}

@FindBy(how= How.LINK_TEXT,using="Find Leads")
WebElement linkFindLead;
public FindLeadsPage clickFindLeadLink()
{
	click(linkFindLead);
	return new FindLeadsPage();
	
}

}