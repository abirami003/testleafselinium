package POM_pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;



public class EditLeadPage extends ProjectMethods
{
public EditLeadPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(how= How.ID,using="updateLeadForm_companyName")
WebElement eLCompanyName;
public EditLeadPage enterELCname(String data) throws IOException
{
	eLCompanyName.clear();
	type(eLCompanyName, data);
	return this;
}

@FindBy(how=How.XPATH, using="//input[@type='submit']")
WebElement buttonUpdateLead;
public ViewLeadPage clickUpdateLeadButton()
{
	click(buttonUpdateLead);
	return new ViewLeadPage();
}
}