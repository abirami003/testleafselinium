package POM_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;



public class ViewLeadPage extends ProjectMethods
{
public ViewLeadPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(how= How.ID,using="viewLead_firstName_sp")
WebElement vLFirstName;
public ViewLeadPage verifyFName(String data)
{
	verifyExactText(vLFirstName, data);
	return this;
}

@FindBy(how= How.ID,using="viewLead_companyName_sp")
WebElement vLCompanyName;
public ViewLeadPage verifyCName(String data)
{
	verifyExactText(vLCompanyName, data);
	return this;
}

@FindBy(how=How.LINK_TEXT, using="Edit")
WebElement buttonEdit;
public EditLeadPage clickEditLeadButton()
{
	click(buttonEdit);
	return new EditLeadPage();
}


@FindBy(how=How.LINK_TEXT, using="Duplicate Lead")
WebElement buttonDuplicate;
public DuplicateLeadPage clickDuplicateLeadButton()
{
	click(buttonDuplicate);
	return new DuplicateLeadPage();
}


@FindBy(how=How.LINK_TEXT, using="Delete")
WebElement buttonDelete;
public MyLeadsPage clickDeleteLeadButton()
{
	click(buttonDelete);
	return new MyLeadsPage();
}
}