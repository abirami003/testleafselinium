package testngAttribute;
import wdMethods.ProjectMethods;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class TC004_EditLead  extends ProjectMethods{
	
	@BeforeTest
	public void beforeTest()
	{
		testCaseName="TC004_Edit Lead";
		testCaseDesc="To Edit lead";
		author="Abi";
		category="Smoke";
		System.out.println("before test");
	}
		@Test
		public void EditLead() throws InterruptedException, IOException {
	
				Thread.sleep(3000);
	click(locateElement("linktext", "Leads"));
	Thread.sleep(3000);
	//Edit lead
	click(locateElement("linktext", "Find Leads"));
	Thread.sleep(3000);
	
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Abirami");
	
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(3000);
	
	click(locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
	
	Thread.sleep(3000);
	driver.findElementByClassName("subMenuButton").click();
	Thread.sleep(1000);
	locateElement("createLeadForm_companyName").clear();
	
	type(locateElement("createLeadForm_companyName"), "Test Leaf");
	
	click(locateElement("xpath","(//input[@name='submitButton'])[1]"));
	
		}

	


}
