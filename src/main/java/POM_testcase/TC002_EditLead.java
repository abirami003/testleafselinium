package POM_testcase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_pages.HomePage;
import POM_pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods 
{
	@BeforeTest(groups = {"common"})
	public void setValuesEditLead()
	{
		testCaseName =  "TC003";
		testCaseDesc =  "Edit Lead";
		author = "Sumathy";
		category = "Smoke";
		ExcelFileName = "EditLead";
	}
		
	@Test(groups = {"sanity"}, /*dependsOnGroups= {"smoke"},*/dataProvider="fetchData")
	public void EditLead(String leadName, String companyName) throws InterruptedException, IOException 
	{
	new MyHomePage()
	.clickLeadsLink()
	.clickFindLeadLink()
	.enterUserName(leadName)
	.clickFindLeadButton()
	.clickFoundLeadLink()
	.clickEditLeadButton()
	.enterELCname(companyName)
	.clickUpdateLeadButton()
	.verifyCName(companyName);
	}
	
	
}
