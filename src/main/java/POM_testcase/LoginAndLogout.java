package POM_testcase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_pages.LoginPage;
import POM_pages.MyHomePage;
import wdMethods.ProjectMethods;

public class LoginAndLogout extends ProjectMethods
{
	@BeforeTest(groups = {"common"})
	public void setValues()
	{
		testCaseName = "TC001";
		testCaseDesc = "Create Lead";
		category = "smoke";		
		ExcelFileName = "LoginDetails";
		author = "Sumathy";
	}
	
	@Test(invocationCount =1 ,groups = {"smoke"}, dataProvider="positive")
	public void Login(String userName, String password) throws IOException 
	{	
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfaLink();
	}
	}
		


