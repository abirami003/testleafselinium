package POM_testcase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods
{
	@BeforeTest(groups = {"common"})
	public void setValuesDuplicateLead()
	{
		testCaseName = "TC005";
		testCaseDesc = "Duplicate Lead";
		author ="Sumathy";
		category = "Smoke";
		ExcelFileName = "DuplicateLead";
	}
	
	@Test(groups = {"regression"},dataProvider="fetchData")
	public void DuplicateLead(String leadEmail) throws InterruptedException, IOException
	{
		new MyHomePage()
		.clickLeadsLink()
		.clickFindLeadLink()
		.clickEmailTab()
		.enterEmail(leadEmail)
		.clickFindLeadButton()
		.clickFoundLeadLink()
		.clickDuplicateLeadButton()
		.clickCreateLeadButton();
	
	}

}
