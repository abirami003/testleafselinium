package POM_testcase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods
{
	@BeforeTest(groups = {"common"})
	public void setValuesDeleteLead()
	{
		testCaseName = "TC004";
		testCaseDesc = "Delete Lead";
		category = "Smoke";
		author =  "Sumathy";
		ExcelFileName = "DeleteLead";
		
	}
	
	
	@Test(groups = {"sanity"},/*dependsOnGroups= {"smoke"},*/dataProvider="fetchData")
public void DeleteLead(String countryCode, String phoneNumber) throws InterruptedException, IOException
{
		new MyHomePage()
		.clickLeadsLink()
		.clickFindLeadLink()
		.clickPhoneTab()
		.enterCountryCode(countryCode)
		.enterPhoneNumber(phoneNumber)		
		.clickFindLeadButton()
		.getFoundLeadTD()
		.clickFoundLeadLink()
		.clickDeleteLeadButton()
		.clickFindLeadLink()
		.seachByLeadId()
		.verifyNoRecordFound();
		
}
}
