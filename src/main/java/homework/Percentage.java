package homework;

import java.util.Scanner;

public class Percentage {
	
	public int Uppercase(String a)
	{
		char [] chr=a.toCharArray();
		int UpperCount=0;
		int length=a.length();
		for(int i=0; i<length;i++) {
            if(Character.isUpperCase(chr[i]))
            {
            	UpperCount++;
            }
        }
		return UpperCount;
	}
		public int Lowercase(String a)
		{
			char [] chr=a.toCharArray();
			int LowerCount=0;
			int length=a.length();
			for(int i=0; i<length;i++) {
	            if(Character.isLowerCase(chr[i]))
	            {
	            	LowerCount++;
	            }
	        }
		
		return LowerCount;
	}
		public int Otherchar(String a)
		{
			
			int Otherchar=0;
			int length=a.length();
			for(int i=0; i<length;i++) {
	            if(Character.isDigit(a.charAt(i)))
	            {
	            	Otherchar++;
	            }
	        }
		
		return Otherchar;
	}
		
		public float Percent(int count,int len)
		{	String str;
			float per;
			
			per=((count*100)/ len);
			
			return per;
			
		}
	public static void main(String[] args) {
		int Ucount=0,Lcount=0,digits=0;
		int length=0;
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter the string");
		String str = scr.nextLine();
		length=str.length();
		Percentage P=new Percentage();
		Ucount=P.Uppercase(str);
		
		Lcount=P.Lowercase(str);
		
		digits=P.Otherchar(str);
		
		//System.out.println(P.Percent((Ucount), length));
		System.out.println("Number of upper case letter is "+Ucount+". so uppper case percentage is "+P.Percent(Ucount, length));
		System.out.println("Number of Lower case letter is "+Lcount+". so lower case percentage is "+P.Percent(Lcount, length));
		System.out.println("Number of digits is "+digits+". so digits percentage is "+(P.Percent(digits, length)));
		
		System.out.println("Number of other character is "+(length-(Ucount+Lcount+digits))+". so other character percentage is "+P.Percent((length-(Ucount+Lcount+digits)), length));
	
	}

}
