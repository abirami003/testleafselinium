package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class DispDigitOfNumber {

	public static void main(String[] args) {
		System.out.println("Enter the value");
		Scanner scr=new Scanner(System.in);
		int num=scr.nextInt();
		char[] number=(""+num).toCharArray();
		List <Character> No=new ArrayList<Character>();
		for (char eachnum : number) {
			
			No.add(eachnum);
		}
		Collections.sort(No);
		 HashSet<Character> hashset = new HashSet<Character>();
		 hashset.addAll(No);
		//System.out.println(hashset);
		for (Character character : hashset) {
			System.out.print(character);
			
		}
	}

}
