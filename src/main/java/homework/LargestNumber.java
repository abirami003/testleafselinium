package homework;

import java.util.Scanner;

public class LargestNumber {
	static int getLLessThanN(int number, int digit)
	{
	   

	    char c = Integer.toString(digit).charAt(0);

	   
	    for (int i = number; i > 0; --i)
	    {
	        if(Integer.toString(i).indexOf(c) == -1)
	        {
	            

	            return i;
	        }
	        
	    }

	    return -1;
	}

	public static void main(String[] args) {
		System.out.println("Enter two number: value and digit");
		Scanner scr=new Scanner(System.in);
		int num = scr.nextInt();
		int digit = scr.nextInt();
		if(digit<0||digit>=10)
		{
			System.out.println("Enter valid single digit positive number");
			digit=scr.nextInt();
		}
		LargestNumber Lnum=new LargestNumber();
		int number=Lnum.getLLessThanN(num, digit);
		System.out.println(number);
		
	}

}
