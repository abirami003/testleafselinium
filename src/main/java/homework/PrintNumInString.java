package homework;

import java.util.Scanner;


public class PrintNumInString {

public static void main(String[] args) {
		
	Scanner scr=new Scanner(System.in);
	System.out.println("Enter the string");
	String str=scr.next();
	str=str.replaceAll("[^\\d]", "");
	if(str.equals(""))
	{
		System.out.println("There is no number in a given string");
	}
	else
	{
	System.out.println(str);
	}
	
}	

}
