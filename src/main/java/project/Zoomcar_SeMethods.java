package project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import wdMethods_old.SeMethods_old;

public class Zoomcar_SeMethods extends SeMethods_old{
	@Test
	public void Zoomcar() throws InterruptedException {
startApp("Chrome","https://www.zoomcar.com/chennai");
	
click(locateElement("xpath", "//a[@title='Start your wonderful journey']"));
	
click(locateElement("xpath", "//div[contains(text(),'Thuraipakkam')]"));

click(locateElement("xpath", "//button[contains(text(),'Next')]"));

	WebElement Tomorrowdate = locateElement("xpath","(//div[@class='days']/div)[2]");
	String date1 = Tomorrowdate.getText();
	System.out.println("date1  "+Tomorrowdate.getText());
	System.out.println("************");
	click(Tomorrowdate);
	
	click(locateElement("xpath", "//button[text()='Next']"));
	Thread.sleep(3000);
	
	WebElement checkdate = locateElement("xpath","(//div[@class='days']/div)[1]");
	String date2 = checkdate.getText();
	System.out.println("date2  "+checkdate.getText());
	System.out.println("************");
	if(date1.equals(date2)){
		System.out.println("Slection confirmed");
		
	}
	
	click(locateElement("xpath", "//button[text()='Done']"));
	
	String xpath="//div[@class='car-item']";
	
	List<WebElement> results = driver.findElementsByXPath(xpath);
	int NoOfResults = results.size();
	System.out.println("No of Results found  "+NoOfResults);
	
	click(locateElement("xpath", "//div[@class='item active-input']"));
	Thread.sleep(5000);
	
	List<WebElement> highPrice = driver.findElementsByXPath("//div[@class='price']");
	List<Integer> highPricevalue =new ArrayList<Integer> ();
	for (WebElement webElement : highPrice) {
		String substring = webElement.getText().substring(2); 
		highPricevalue.add(Integer.parseInt(substring));
	}
	
	Collections.sort(highPricevalue);
	int sizeofHighVal = highPricevalue.size();
	Integer maxval = highPricevalue.get(sizeofHighVal-1);
	String carname = locateElement("xpath","(//div[contains(text(),'"+maxval+"')]/parent::div/parent::div/preceding-sibling::div)[2]/h3").getText();
	
	System.out.println("The maximum price is "+maxval+" and the car name is "+carname);
	
	//div[contains(text(),'"+maxval+"')]
	
	click(locateElement("xpath", "//div[contains(text(),'"+maxval+"')]/following-sibling::button"));
	closeAllBrowsers();
		
	}
}