package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

public class CreateLead {
	@Test(invocationCount=2)
	public void CreateLead1() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
driver.manage().window().maximize();
//Implicit wait
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//Loading URL
driver.get("http://leaftaps.com/opentaps/control/login");
//Login

driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
//select CRM/SFA link
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Leads").click();
Thread.sleep(3000);
//create Lead
driver.findElementByLinkText("Create Lead").click();
driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
String n1=driver.findElementById("createLeadForm_companyName").getAttribute("maxlength");
System.out.println(n1);
String n2=driver.findElementById("createLeadForm_companyName").getAttribute("name");
System.out.println(n2);
driver.findElementById("createLeadForm_firstName").sendKeys("Abirami");
driver.findElementById("createLeadForm_firstName").sendKeys(" M");
driver.findElementById("createLeadForm_lastName").sendKeys("Murugaiyan");
driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Abi");
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("M");
driver.findElementById("createLeadForm_personalTitle").sendKeys("Welcome");
WebElement srcId = driver.findElementById("createLeadForm_dataSourceId");
Select slct=new Select(srcId);
slct.selectByVisibleText("Direct Mail");
driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Selenium Aug");
driver.findElementById("createLeadForm_annualRevenue").sendKeys("3,00,000");
WebElement Industry = driver.findElementById("createLeadForm_industryEnumId");
Select slct1=new Select(Industry);
slct1.selectByVisibleText("Computer Software");
WebElement OwnerShip = driver.findElementById("createLeadForm_ownershipEnumId");
Select slct2=new Select(OwnerShip);
slct2.selectByVisibleText("Public Corporation");
driver.findElementById("createLeadForm_sicCode").sendKeys("99");
driver.findElementById("createLeadForm_description").sendKeys("createLeadForm_description");
driver.findElementById("createLeadForm_importantNote").sendKeys("createLeadForm_importantNote");
driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("IN");
driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("641104");

//driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("641104");
//driver.findElementByXPath("//input[@class='smallSubmit']").click();

//
driver.close();
	}
	

}
