package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DeleteLead {
	@Test(dependsOnMethods= {"week4.day1.CreateLead.CreateLead1"})
public void DeleteLead1() throws InterruptedException {
	
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	
	ChromeDriver driver=new ChromeDriver();
driver.manage().window().maximize();
//Implicit wait
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//Loading URL
driver.get("http://leaftaps.com/opentaps/control/login");
//Login

driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
//select CRM/SFA link
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Leads").click();
Thread.sleep(3000);


//Delete lead
driver.findElementByLinkText("Find Leads").click();
Thread.sleep(3000);
driver.findElementByXPath("//span[text()='Phone']").click();
Thread.sleep(2000);
driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("98");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(10000);
WebElement delete = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
String del = delete.getText();
delete.click();
Thread.sleep(3000);
driver.findElementByLinkText("Delete").click();
Thread.sleep(3000);
driver.findElementByLinkText("Find Leads").click();
driver.findElementByXPath("//input[@name='id']").sendKeys(del);
driver.findElementByXPath("//button[text()='Find Leads']").click();
driver.close();
}
}
