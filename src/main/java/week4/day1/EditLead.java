package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead {
	@Test(enabled=false)
	public void EditLead1() throws InterruptedException {
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
driver.manage().window().maximize();
//Implicit wait
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//Loading URL
driver.get("http://leaftaps.com/opentaps/control/login");
//Login

driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
//select CRM/SFA link
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Leads").click();
Thread.sleep(3000);
//Edit lead
driver.findElementByLinkText("Find Leads").click();
Thread.sleep(3000);
driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Abirami");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
Thread.sleep(3000);
driver.findElementByClassName("subMenuButton").click();
Thread.sleep(1000);
driver.findElementById("createLeadForm_companyName").clear();
driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
driver.close();
	}

}
