package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FramesAndAlerts {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/js_popup.asp");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("(//a[text()=\"Try it Yourself �\"])[3]").click();
		//WebElement frame = driver.findElementById("iframeResult");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']");
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Abirami");
		alert.accept();
		WebElement txt = driver.findElementById("demo");
		System.out.println(txt.getText());
		driver.switchTo().defaultContent();
		
		//driver.close();
	}
}
