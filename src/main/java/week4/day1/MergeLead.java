package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();

		//Maximize the window
driver.manage().window().maximize();
//Implicit wait
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//Loading URL
driver.get("http://leaftaps.com/opentaps/control/login");
//Login

driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
//select CRM/SFA link
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Leads").click();
Thread.sleep(3000);
driver.findElementByLinkText("Merge Leads").click();
//driver.findElementByLinkText("Merge Leads").click();
Thread.sleep(3000);
Set<String> windowHandles = driver.getWindowHandles();
List<String> lst=new ArrayList<>();
lst.addAll(windowHandles);
driver.findElementByXPath("(//input[@id='ComboBox_partyIdFrom']//following::img)[1]").click();
windowHandles = driver.getWindowHandles();
lst=new ArrayList<>();
lst.addAll(windowHandles);
driver.switchTo().window(lst.get(1));
driver.findElementByXPath("//input[@name='id']").sendKeys("10");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
WebElement Lead1 = driver.findElementByXPath("((//div[text()='Lead ID']//following::tr)[1]/td)[1]");
String text=Lead1.getText();

Lead1.click();
System.out.println(text);
driver.switchTo().window(lst.get(0));
driver.findElementByXPath("(//input[@id='ComboBox_partyIdFrom']//following::img)[2]").click();
driver.switchTo().window(lst.get(1));
driver.findElementByXPath("//input[@name='id']").sendKeys("10215");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
driver.findElementByXPath("((//div[text()='Lead ID']//following::tr)[1]/td)[1]").click();;
driver.switchTo().window(lst.get(0));
driver.findElementByLinkText("Merge").click();
driver.switchTo().alert().accept();
String line1 = driver.findElementByXPath("//div[@class='messages']/div").getText();
String line2 = driver.findElementByXPath("(//div[@class='messages']/div//following::li)[1]").getText();
String line3 = driver.findElementByXPath("(//div[@class='messages']/div//following::li)[2]").getText();
System.out.println(line1+"\n"+line2+"\n"+line3);
driver.findElementByLinkText("Find Leads").click();
Thread.sleep(3000);
driver.findElementByXPath("//input[@name='id']").sendKeys("10");
driver.findElementByXPath("//button[text()='Find Leads']").click();

driver.close();
	}
}
