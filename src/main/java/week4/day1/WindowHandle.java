package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(windowHandles);
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		windowHandles = driver.getWindowHandles();
		lst=new ArrayList<>();
		lst.addAll(windowHandles);
		
		
		driver.findElementByPartialLinkText("Contact Us").click();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		windowHandles = driver.getWindowHandles();
		lst=new ArrayList<>();
		lst.addAll(windowHandles);
		
		
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		driver.switchTo().window(lst.get(0));
		File src=driver.getScreenshotAs(OutputType.FILE);
		File Des=new File("./Snaps/irctc.png");
		FileUtils.copyFile(src, Des);
	}
	

}
